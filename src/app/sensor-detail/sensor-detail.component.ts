import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ISensorData} from '../sensordata/sensor-data';

@Component({
  templateUrl: './sensor-detail.component.html',
  styleUrls: ['./sensor-detail.component.scss']
})
export class SensorDetailComponent implements OnInit {
  pageTitle = 'Sensor Detail';
  sensorData: ISensorData = {
    sensor: 'top',
    temperature: 24.9,
    humidity: 34.9,
    illuminance: 55.8,
    windSpeed: 3.3
  };

  constructor(private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    const sensorName = this.route.snapshot.paramMap.get('id');
    this.pageTitle += `: ${sensorName}`;
  }

  onBack(): void {
    this.router.navigate(['/sensors']);
  }
}
