import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {NavigationComponent} from './navigation/navigation.component';
import {SupportComponent} from './support/support.component';
import {ControlsComponent} from './controls/controls.component';
import {SensorModule} from './sensordata/sensor.module';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    SupportComponent,
    ControlsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: 'support', component: SupportComponent},
      {path: 'controls', component: ControlsComponent},
      {path: '', redirectTo: 'sensors', pathMatch: 'full'},
      {path: '**', redirectTo: 'sensors', pathMatch: 'full'},
    ]),
    SensorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
