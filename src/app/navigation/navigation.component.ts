import {Component, OnInit} from '@angular/core';
import {INavigationRoute} from './INavigationRoute';

@Component({
  selector: 'gh-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  pageTitle = 'Green House';
  navigationRoutes: INavigationRoute[] = [
    {
      routerLink: '/sensors',
      routerName: 'Sensors'
    },
    {
      routerLink: '/controls',
      routerName: 'Controls'
    },
    {
      routerLink: '/support',
      routerName: 'Support'
    }
  ];

  constructor() {
  }

  ngOnInit(): void {
  }
}
