export interface INavigationRoute {
  routerLink: string;
  routerName: string;
}
