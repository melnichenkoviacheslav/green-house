import {Component, OnDestroy, OnInit} from '@angular/core';
import {ISensorData} from './sensor-data';
import {SensorDataService} from './sensor-data.service';
import {Subscription} from 'rxjs';

@Component({
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.scss']
})

export class SensorsComponent implements OnInit, OnDestroy {
  showStatus = false;
  sensorData: ISensorData[] = [];
  filteredSensorData: ISensorData[] = [];
  private sensorFilterField = '';
  private errorMessage: any;
  private sub: Subscription | undefined;

  constructor(private sensorDataService: SensorDataService) {
  }

  get sensorFilter(): string {
    return this.sensorFilterField;
  }

  set sensorFilter(value: string) {
    this.sensorFilterField = value;
    this.filteredSensorData = this.performFilter(value);
  }

  performFilter(filterBy: string): ISensorData[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.sensorData.filter((sensorData: ISensorData) =>
      sensorData.sensor.toLocaleLowerCase().includes(filterBy));
  }

  toggleStatus(): void {
    this.showStatus = !this.showStatus;
  }

  ngOnInit(): void {
    this.sub = this.sensorDataService.getSensorData().subscribe(
      {
        next: sensorData => {
          this.sensorData = sensorData;
          this.filteredSensorData = this.sensorData;
        },
        error: err => this.errorMessage = err
      }
    );
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
