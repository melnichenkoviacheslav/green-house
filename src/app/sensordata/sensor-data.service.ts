import {Injectable} from '@angular/core';
import {ISensorData} from './sensor-data';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SensorDataService {
  private sensorDataUrl = 'api/sensors/sensor-data.json';

  constructor(private http: HttpClient) {
  }

  getSensorData(): Observable<ISensorData[]> {
    return this.http.get<ISensorData[]>(this.sensorDataUrl).pipe(
      tap(data => console.log('All:', JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
