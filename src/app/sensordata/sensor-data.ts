export interface ISensorData {
  sensor: string;
  temperature: number | null;
  humidity: number | null;
  illuminance: number | null;
  windSpeed: number | null;
}
