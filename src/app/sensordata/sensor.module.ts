import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SensorsComponent} from './sensors.component';
import {SensorDetailComponent} from '../sensor-detail/sensor-detail.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    SensorsComponent,
    SensorDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
        {path: 'sensors', component: SensorsComponent},
        {path: 'sensors/:id', component: SensorDetailComponent}
      ]
    )]
})
export class SensorModule {
}
