FROM node:14.16.1-alpine3.13 As builder

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm install

COPY . .

RUN npm run build --prod

FROM nginx:stable-alpine

COPY --from=builder /usr/src/app/dist/green-house/ /usr/share/nginx/html
